---
layout: post
title: "Try the Octopress Plugin - 1"
date: 2014-04-29 10:49:56 -0700
comments: true
autor: Ray
categories: [octopress, plugin, video, image, quote]
---
Today I will explain and try some of the basic Octopress Pulgins.

> There are two ways to escape without plugins:
>
- use the code below:```{%raw%}{{ "{% this " }} %}```{% endraw %}
- use the ```{{ "{% raw "}} %} {{ "{% this " }} %} {{ "{% endraw " }} %}```


###1. Video Tag###
This plugin makes it easy to insert mp4 encoded HTML5 videos in a post. Octopress ships with javascripts which detect mp4 video support (using Modernizr) and automatically offer a flash player fallback.

**Syntax:**
```
{{ "{% video url/to/video [width height] [url/to/poster]" }} %}
```
**Example:** 
```
{{ "{% video http://s3.imathis.com/video/zero-to-fancy-buttons.mp4 640 320 http://s3.imathis.com/video/zero-to-fancy-buttons.png " }}%}
```

{% video http://s3.imathis.com/video/zero-to-fancy-buttons.mp4 640 320 http://s3.imathis.com/video/zero-to-fancy-buttons.png %}


###2. Image Tag###
This plugin makes it easy to insert images in a post, with optional class names, alt and title attributes.

**Syntax:**
```
{{ "{% img [class names] /path/to/image [width] [height] [title text [alt text]]" }} %}
```

**Example:** 
{%raw%}
```
{% img http://placekitten.com/890/280 %}
{% img left http://placekitten.com/320/250 Place Kitten #2 %}
{% img right http://placekitten.com/300/500 150 250 Place Kitten #3 %}
{% img right http://placekitten.com/300/500 150 250 'Place Kitten #4' 'An image of a very cute kitten' %}
```
{%endraw%}

{% img left http://placekitten.com/320/250 Place Kitten #2 %}
{% img right http://placekitten.com/300/500 150 250 Place Kitten #3 %}
{% img right http://placekitten.com/300/500 150 250 'Place Kitten #4' 'An image of a very cute kitten' %}
{% img http://placekitten.com/890/280 %}


###3. Blockquote###
The blockquote plugin takes and author, source, title and quote, and outputs semantic HTML.

**Syntax**
{%raw%}
```
{% blockquote [author[, source]] [link] [source_link_title] %}
Quote string
{% endblockquote %}
```
{%endraw%}


