---
layout: post
title: "Changing Magento Base URLs"
date: 2014-08-23 13:40:30 -0700
comments: true
categories: [magento, php]
---

This article provides the method to change Magento Base URLs through PHPMyAdmin and through the command line. 

##Method One: Changing Magento Base URLs in Magento Backend

1. Log into your Magento backend.

	{% img http://docs.nexcess.net/assets/Screenshots_From_Dev_Documents/Magento_Admin_panel.png %}

2. On the top menu bar, **click System > Configuration**.

	{% img http://docs.nexcess.net/assets/Screenshots_From_Dev_Documents/Magento_System_Configuration_V2.png %}

3. On the left hand menu, click Web.

	{% img http://docs.nexcess.net/assets/Screenshots_From_Dev_Documents/Magento_Secure_unsecure_V2.png %}

4. Open the Unsecure and Secure dropdowns, and locate the base URL line, and replace this with the new URL.

5. Clear the Magento cache and your browser's cache, and the site should load properly now.

##Method Two: Changing Magento Base URLs Through PHPMyAdmin

1. Log into your godaday Cpanel 
 
2. Click the **PhpMyAdmin** 

	{% img http://docs.nexcess.net/assets/Screenshots_From_Dev_Documents/MageURL_MainMyAdminPage.png %}

3. You will be brought to the main menu for phpMyAdmin.  On the left hand side of the screen, locate and click the name of the Magento database you're looking to make the URL changes for.

	{% img http://docs.nexcess.net/assets/Screenshots_From_Dev_Documents/MageURL_Locate_core_config_data.png %}

4. You will be presented with a list of all the tables in the database, in alphabetical order.  Search for the ```core_config_data table```, and press the Browse button next to the name. 

5. Expand the viewing area of phpmyadmin.  At the top and bottom in the main panel, locate the grey box, and change the Number of Rows to a larger number, like 100.

	{% img http://docs.nexcess.net/assets/Screenshots_From_Dev_Documents/MageURL_Change_Base_URLs.png %}

6. Locate the rows ```web/unsecure/base_url``` and ```web/secure/base_url``` and click the edit button next to the corresponding lines.  Change the base URL to the intended string, and press ok. 

7. Flush the Magento cache, and the site should load with the set base URLs properly now. **(Simple delete the var/cache/)**

##Method Three: Changing Magento Base URLs Through Command Line

1. Log into the client's server using terminal.
2. Gain access to MySQL using ```m``` or ```mysql -u root -p```.
3. Access their database using: ```use database``` ;

	where ```database``` is the database name.

4. Run the following command:  ```select * from core_config_data where path like '%base%url%';```
5. This will display the current base_urls set in magento.
6. To change the base URLs run:

```
update core_config_data set value = 'http://domainname/' where path = 'web/unsecure/base_url'; 
update core_config_data set value = 'http://domainname/' where path = 'web/secure/base_url'; 
```


